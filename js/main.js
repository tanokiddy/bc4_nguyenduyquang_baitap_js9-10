const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

//Chức năng thêm nhân viên
var dsnv = [];
//Lấy thông tin từ localstorage

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
if (dsnvJson != null) {
  dsnv = JSON.parse(dsnvJson);
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.hoTen,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCoBan,
      nv.chucVu,
      nv.gioLam
    );
  }
  renderDSNV(dsnv);
}

function themNV() {
  //display sp-thongbao to "inline"
  document
    .querySelectorAll(".sp-thongbao")
    .forEach((a) => (a.style.display = "inline"));
  var newNv = layThongTinTuForm();
  //check taiKhoan
  var isValid =
    validator.kiemTraRong(
      newNv.taiKhoan,
      "tbTKNV",
      "Tài khoản không không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newNv.taiKhoan,
      "tbTKNV",
      "Tài khoản phải từ 4-6 ký tự",
      4,
      6
    ) &&
    validator.kiemTraSo(
      newNv.taiKhoan,
      "tbTKNV",
      " Tài khoản phải là ký tự số"
    );
  //check tenNV
  isValid &=
    validator.kiemTraRong(
      newNv.hoTen,
      "tbTen",
      "Họ và tên không được để trống"
    ) && validator.kiemTraChu(newNv.hoTen, "tbTen", "Họ và tên không hợp lệ");
  //check email
  isValid &=
    validator.kiemTraRong(
      newNv.email,
      "tbEmail",
      "Email không được để trống"
    ) &&
    validator.kiemTraEmail(
      newNv.email,
      "tbEmail",
      "Email không đúng định dạng"
    );
  //check matKhau
  isValid &=
    validator.kiemTraRong(
      newNv.matKhau,
      "tbMatKhau",
      "Mật khẩu không được để trống"
    ) &&
    validator.kiemTraMatKhau(
      newNv.matKhau,
      "tbMatKhau",
      "Mật khẩu phải từ 6-10 ký tự, bao gồm 1 ký tự in hoa, 1 ký tự số và 1 ký tự đặc biệt"
    );
  //check ngayLam
  isValid &=
    validator.kiemTraRong(
      newNv.ngayLam,
      "tbNgay",
      "Ngày làm không được để trống"
    ) &&
    validator.kiemTraDate(
      newNv.ngayLam,
      "tbNgay",
      "Vui lòng nhập hoặc chọn đúng định dạng mm/dd/yyyy"
    );
  //check luongCoBan
  isValid &=
    validator.kiemTraRong(
      newNv.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không được để trống"
    ) &&
    validator.kiemTraGiaTri(
      newNv.luongCoBan,
      "tbLuongCB",
      "Lương cơ bản không hợp lệ",
      1000000,
      20000000
    );
  //check chucVu
  isValid &= validator.kiemTraChucVu(
    newNv.chucVu,
    "tbChucVu",
    "Vui lòng chọn đúng chức vụ"
  );
  //check gioLam
  isValid &=
    validator.kiemTraRong(
      newNv.gioLam,
      "tbGioLam",
      "Giờ làm không được để trống"
    ) &&
    validator.kiemTraGiaTri(
      newNv.gioLam,
      "tbGioLam",
      "Giờ làm không hợp lệ",
      80,
      200
    );

  if (isValid) {
    dsnv.push(newNv);
    // Tạo json
    var dsnvJson = JSON.stringify(dsnv);
    // Lưu json
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
    document.getElementById("btnThemNV").setAttribute("data-dismiss", "modal");
  } else {
    document
      .getElementById("btnThemNV")
      .removeAttribute("data-dismiss", "modal");
  }
}

function xoaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    dsnv.splice(index, 1);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    var nv = dsnv[index];
    showThongTinTuForm(nv);
  }
  document.getElementById("tknv").readOnly = true;
  document.querySelector("#btnThemNV").disabled = true;
}

function searchNV() {
  var id = document.querySelector("#searchName").value;
  var index = timKiemViTriTheoXepLoai(id, dsnv);
  if (index != -1) {
    var nv = dsnv[index];
    var trContent = `<tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.hoTen}</td>
    <td>${nv.email}</td>
    <td>${nv.ngayLam}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tongLuong()}</td>
    <td>${nv.xepLoai()}</td>
    <td>
    <button onclick="xoaNhanVien('${
      nv.taiKhoan
    }')" class="btn btn-danger btn-sm">Xoá</button>
    <br>
    <button onclick="suaNhanVien('${
      nv.taiKhoan
    }')" data-toggle="modal" data-target="#myModal" class="btn btn-warning btn-sm">Sửa</button>
    </td>  
    `;
  } else {
    trContent = `<p>Không tìm thấy</p>`;
  }
  document.querySelector("#tableDanhSach").innerHTML = trContent;
  //tableDanhSach
}
function updateNV() {
  themNV();
  document.getElementById("tknv").readOnly = false;
  document.querySelector("#btnThemNV").disabled = false;
  var id = document.querySelector("#tknv").value;
  xoaNhanVien(id);
}

document.querySelector("#btnThem").onclick = function () {
  document.querySelector("#tknv").value = "";
  document.querySelector("#tknv").readOnly = false;
  document.querySelector("#name").value = "";
  document.querySelector("#email").value = "";
  document.querySelector("#password").value = "";
  document.querySelector("#luongCB").value = "";
  document.querySelector("#gioLam").value = "";
  document.querySelector("#chucVu").value = "Chọn chức vụ";
  document.querySelector("#btnThemNV").disabled = false;
};
