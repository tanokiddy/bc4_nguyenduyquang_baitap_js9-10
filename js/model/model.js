function NhanVien(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    var totalWage = "";
    if (chucVu == "Giám đốc") {
      totalWage = luongCoBan * 3;
    } else if (chucVu == "Trưởng phòng") {
      totalWage = luongCoBan * 2;
    } else if (chucVu == "Nhân viên") {
      totalWage = luongCoBan * 1;
    } else {
      totalWage = "Vui lòng chọn lại chức vụ";
    }
    return totalWage;
  };
  this.xepLoai = function () {
    var rankNV = "";
    if (gioLam * 1 > 0 && gioLam * 1 < 160) {
      rankNV = "Trung bình";
    } else if (gioLam * 1 >= 160 && gioLam * 1 < 176) {
      rankNV = "Khá";
    } else if (gioLam * 1 >= 176 && gioLam * 1 < 192) {
      rankNV = "Giỏi";
    } else if (gioLam * 1 > 192) {
      rankNV = "Xuất sắc";
    }
    return rankNV;
  };
}
