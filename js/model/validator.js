var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  kiemTraDiem: function (value, idError, message) {
    if (value < 0 || value > 10) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraSo: function (value, idError, message) {
    if (Number.isInteger(value * 1)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  kiemTraChu: function (value, idError, message) {
    const char =
      /^[A-ZÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐa-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]+$/;
    if (char.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  //   kiemTraSoChu: function (value, idError, message) {
  //     const alphanum = /^[A-Za-z0-9]+$/;
  //     if (alphanum.test(value)) {
  //       document.getElementById(idError).innerText = "";
  //       return true;
  //     } else {
  //       document.getElementById(idError).innerText = message;
  //       return false;
  //     }
  //   },
  kiemTraMatKhau: function (value, idError, message) {
    const pw = /^(?=.*[0-9])(?=.*[A-Z])(?=.*\W)(?!.* ).{6,10}$/;
    if (pw.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      return false;
    }
  },
  kiemTraDate: function (value, idError, message) {
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(value)) {
      document.getElementById(idError).innerText = message;
      return false;
    }
    // Parse the date parts to integers
    var parts = value.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12) {
      document.getElementById(idError).innerText = message;
      return false;
    }

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
  },
  kiemTraGiaTri: function (value, idError, message, min, max) {
    if (value * 1 < min || value * 1 > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChucVu: function (value, idError, message) {
    if (value == "Chọn chức vụ" || value == "") {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
};
