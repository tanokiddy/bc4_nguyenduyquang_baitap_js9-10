function layThongTinTuForm() {
  const taiKhoan = document.querySelector("#tknv").value;
  const hoTen = document.querySelector("#name").value;
  const email = document.querySelector("#email").value;
  const matKhau = document.querySelector("#password").value;
  const ngayLam = document.querySelector("#datepicker").value;
  const luongCoBan = document.querySelector("#luongCB").value;
  const chucVu = document.querySelector("#chucVu").value;
  const gioLam = document.querySelector("#gioLam").value;

  return new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
}

//render array nv ra giao dien
function renderDSNV(nvArr) {
  //contentHTML: chuỗi chứa các thẻ <tr></tr>
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    //trContent: thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
  <td>${nv.taiKhoan}</td>
  <td>${nv.hoTen}</td>
  <td>${nv.email}</td>
  <td>${nv.ngayLam}</td>
  <td>${nv.chucVu}</td>
  <td>${nv.tongLuong()}</td>
  <td>${nv.xepLoai()}</td>
  <td class="d-flex p-4">
  <button onclick="xoaNhanVien(${
    nv.taiKhoan
  })" class="btn btn-sm btn-danger">Xoá</button>
  <button onclick="suaNhanVien(${
    nv.taiKhoan
  })" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-warning ml-1">Sửa</button>
  </td>
  </tr>
  `;
    contentHTML += trContent;
  }
  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
  //tableDanhSach
  return contentHTML;
}

function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == id) {
      return index;
    }
  }
  // ko tìm thấy
  return -1;
}

function showThongTinTuForm(nv) {
  document.querySelector("#tknv").value = nv.taiKhoan;
  document.querySelector("#name").value = nv.hoTen;
  document.querySelector("#email").value = nv.email;
  document.querySelector("#password").value = nv.matKhau;
  document.querySelector("#datepicker").value = nv.ngayLam;
  document.querySelector("#luongCB").value = nv.luongCoBan;
  document.querySelector("#chucVu").value = nv.chucVu;
  document.querySelector("#gioLam").value = nv.gioLam;
}

function timKiemViTriTheoXepLoai(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.xepLoai() == id) {
      return index;
    }
  }
  // ko tìm thấy
  return -1;
}
